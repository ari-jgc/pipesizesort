package com.jgc.sizesort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Sample Pipe Size Sorting
 * 
 * @author Arihito Taniguchi 2019-10-14
 */
public class Main {
    
    public static void main(String[] args) {
        
        String[] sizes = { "1-1/2\"", "3\"", "3/4\"", "1/2\"", "1\"", "2\"", "1/4\"" };
        
        for (String s : sizes) {
            extractTest(s);
        }

        Set<PipeSize> pipes = new TreeSet<>();
        for (String s : sizes) {
            pipes.add(new PipeSize(s));
        }

        pipes.forEach(o -> System.out.println(o.getSize()));
    }
    
    private static void extractTest(String str) {
        System.out.printf("### %s ###\n", str);
        String s = "(\\d+\\.?\\d*(?! *\\/))? *-? *(?:(\\d+)*\\/*(\\d+))";
        Pattern p = Pattern.compile(s);
        Matcher m = p.matcher(str);
        if (m.find()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                String g = m.group(i);
                System.out.println("group(" + i + ")=" + g);
            }
        }
    }
}
