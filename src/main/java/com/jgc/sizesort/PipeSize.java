package com.jgc.sizesort;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Sample PipeSize class
 * 
 * @author Arihito Taniguchi 2019-10-14
 */
public class PipeSize implements Comparable<PipeSize> {
    private String size;
    public PipeSize(String size) {
        this.size = size;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public int compareTo(PipeSize o) {
        return Double.compare(toNumber(this.size), toNumber(o.size));
    }
    
    private double toNumber(String size) {
        String s = "(\\d+\\.?\\d*(?! *\\/))? *-? *(?:(\\d+)*\\/*(\\d+))";
        Pattern p = Pattern.compile(s);
        Matcher m = p.matcher(size);
        if (m.find()) {
            if (m.group(0).contains("/")) {
                if (m.group(1) == null) {
                    return Double.parseDouble(m.group(2))
                            / Double.parseDouble(m.group(3));
                } else {
                    return Double.parseDouble(m.group(1))
                            + Double.parseDouble(m.group(2))
                            / Double.parseDouble(m.group(3));
                }
            } else {
                return Double.parseDouble(m.group(0));
            }
        }
        return 0;
    }
}
