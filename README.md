# Pipe Size Sort

## 1. Extract a substring by matching a regular expression

A regular expression (sometimes called a rational expression) is a sequence of 
characters that define a search pattern, mainly for use in pattern matching with 
strings, or string matching, i.e. “find and replace”-like operations.(Wikipedia).

Regular expressions are a generalized way to match patterns with sequences of 
characters. It is used in every programming language like C++, Java and Python. 

Supply a regular expression (regex) to the Pattern class, use the find method of 
the Matcher class to see if there is a match, then use the group method to 
extract the actual group of characters from the String that matches your regular 
expression.

The following stub extracts pipe size (e.g. 1-1/2 from 1-1/2") and split into 4 groups (e.g. 1-1/2, 1, 1, 2).

```java
private static void extractTest(String str) {
    System.out.printf("### %s ###\n", str);
    String s = "(\\d+\\.?\\d*(?! *\\/))? *-? *(?:(\\d+)*\\/*(\\d+))";
    Pattern p = Pattern.compile(s);
    Matcher m = p.matcher(str);
    if (m.find()) {
        for (int i = 0; i <= m.groupCount(); i++) {
            String g = m.group(i);
            System.out.println("group(" + i + ")=" + g);
        }
    }
}
```

The following shows example of extracting substrings by matching a regular expression from the given pipe size.

```
### 1-1/2" ###
group(0)=1-1/2
group(1)=1
group(2)=1
group(3)=2
### 3" ###
group(0)=3
group(1)=null
group(2)=null
group(3)=3
### 3/4" ###
group(0)=3/4
group(1)=null
group(2)=3
group(3)=4
```

If the group(0) contains '/', the given pipe size is a mixed fraction. In this case, each group represents as follows:
* group(1) - a whole number
* group(2) - a numerator
* group(3) - a denominator

If not, the group(0) represents pipe size in real number.

The following stub converts a pipe size to real number.

```java
private double toNumber(String size) {
    String s = "(\\d+\\.?\\d*(?! *\\/))? *-? *(?:(\\d+)*\\/*(\\d+))";
    Pattern p = Pattern.compile(s);
    Matcher m = p.matcher(size);
    if (m.find()) {
        if (m.group(0).contains("/")) {
            if (m.group(1) == null) {
                return Double.parseDouble(m.group(2))
                        / Double.parseDouble(m.group(3));
            } else {
                return Double.parseDouble(m.group(1))
                        + Double.parseDouble(m.group(2))
                        / Double.parseDouble(m.group(3));
            }
        } else {
            return Double.parseDouble(m.group(0));
        }
    }
    return 0;
}
```

## 2. TreeSet in Java
TreeSet is one of the most important implementations of the SortedSet interface 
in Java that uses a Tree for storage. The ordering of the elements is maintained 
by a set using their natural ordering whether or not an explicit comparator is 
provided. This must be consistent with equals if it is to correctly implement 
the Set interface. It can also be ordered by a Comparator provided at set 
creation time, depending on which constructor is used.

## 3. Java Comparable Interface

Java Comparable interface is used to compare objects and sort them according to 
the natural order. Natural ordering is referred to as its compareTo() function. 
The string objects are sorted lexicographically, and the wrapper class objects 
are sorted according to their built-in compareTo() function (like how Integers 
objects are sorted in ascending order).

The compareTo() function compares the current object with the provided object. 
This function is already implemented for default wrapper classes and primitive 
data types; but, this function also​ needs to be implemented for user-defined 
classes.

It returns:

* a positive integer, if the current object is greater than the provided object.
* a negative integer, if the current object is less than the provided object.
* zero, if the current object is equal to the provided object.

The following stub is an example of implementation of compareTo() method using toNumber() method.

```java
@Override
public int compareTo(PipeSize o) {
    return Double.compare(toNumber(this.size), toNumber(o.size));
}
```

